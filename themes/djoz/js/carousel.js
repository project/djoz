(function ($, Drupal) {

  Drupal.behaviors.DJozEventSlider = {
    attach: function (context, settings) {

      $(".event__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 3,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        responsive: {
          992: {
            items: 3,
          },
          768: {
            items: 2,
          },
          0: {
            items: 1,
          },
        }
      });
    },
  };

  Drupal.behaviors.DJozVideoSlider = {
    attach: function (context, settings) {

      $(".videos__slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 4,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        responsive: {
          992: {
            items: 4,
          },
          768: {
            items: 3,
          },
          576: {
            items: 2,
          },
          0: {
            items: 1,
          }
        }
      });
    },
  };

})(jQuery, Drupal);
