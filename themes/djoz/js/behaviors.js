(function ($, Drupal) {
  Drupal.behaviors.DJozCustomBehaviour = {
    attach: function (context, settings) {
      // Custom JS here.
    },
  };

  Drupal.behaviors.DJozCutomFooterAndCountDownHeight = {
    attach: function (context, settings) {

      // Set custom height for footer and countdown.
      function setCustomHeight() {
        let footer_height = $(".footer--home").innerHeight();
        let footer_height_abs = -Math.abs(footer_height);
        $(".footer--home").css("margin-top", footer_height_abs);
        $(".countdown--home").css("padding-bottom", footer_height);
      }

      // Set custom height for the first time.
      setCustomHeight();

      // Set custom height while window is resize.
      $(window).resize(function () {
        setCustomHeight();
      });
    },
  };

})(jQuery, Drupal);
