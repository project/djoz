(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.DJozCountdown = {
    attach: function (context, settings) {
      let countdownDate = drupalSettings.countdownDate;
      $("#countdown-timer").countdown(countdownDate, function(event) {
        $(this).html(event.strftime("<div class='countdown__item'><span>%D</span> <p>Days</p> </div>"
          + "<div class='countdown__item'><span>%H</span> <p>Hours</p> </div>"
          + "<div class='countdown__item'><span>%M</span> <p>Minutes</p> </div>"
          + "<div class='countdown__item'><span>%S</span> <p>Seconds</p> </div>"));
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
