(function ($, Drupal) {

  Drupal.behaviors.DJozGallery = {
    attach: function (context, settings) {
      $('.gallery').masonry({
        // options
        percentPosition: true,
        itemSelector: '.grid-item'
      });
    },
  };
})(jQuery, Drupal);
