'use strict';

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var gulpCopy = require('gulp-copy');
var replace = require('gulp-replace');
var $ = require('gulp-load-plugins')();
var refresh = require('gulp-refresh');

// provide a path to node modules.
var sassPaths = [
  'node_modules'
];

gulp.task('sass', function () {
  return gulp.src(['sass/**/**.scss'])
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.identityMap())
    .pipe($.sass({
        includePaths: sassPaths
      })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('css'))
    .pipe(refresh());
});

gulp.task('default', ['sass'], function () {
  refresh.listen();
  gulp.watch(['sass/**/*.scss'], ['sass']);
});
